package view;

import java.awt.BorderLayout;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import com.teamdev.jxmaps.*;
import com.teamdev.jxmaps.GeocoderRequest;
import com.teamdev.jxmaps.GeocoderResult;
import com.teamdev.jxmaps.GeocoderStatus;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapViewOptions;

import map.StreetViewMap;

public class StreetViewSingleton {
	/*
	 * private static StreetViewSingleton instance; StreetViewMap map; static {
	 * instance = new StreetViewSingleton(); } private StreetViewSingleton() {
	 * 
	 * } public static StreetViewSingleton getInstance() { return instance; } public
	 * void setStreetViewMap(StreetViewMap sample) { map = sample; }
	 * 
	 * public StreetViewMap getMap() { return map; }
	 */

	public static void drawPoints(LinkedList<LatLng> markers) {
		MapViewOptions mvo = new MapViewOptions();
		mvo.importPlaces();
		// mvo.setApiKey("7a1y3jpxlg70k7covavndushu12jznd6puexhj0sc1q1truqbyyz5afwnqpkmtidk1kkspfoc9z0zt25");
		mvo.setApiKey("AIzaSyBj-RnKqf32h8bvdu15CWhO0K3zyt5eXw8");
		StreetViewMap mapView = new StreetViewMap();

		JFrame frame = new JFrame("Street View");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.add(mapView, BorderLayout.CENTER);
		frame.setSize(1600, 1000);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		mapView.setOnMapReadyHandler(new MapReadyHandler() {
			@Override
			public void onMapReady(MapStatus status) {
				System.out.println(" onMapReady "+status);

				if (status == MapStatus.MAP_STATUS_OK) {
					final Map map = mapView.getMap();
					map.setCenter(markers.get(0));

					map.setZoom(5.0);
					GeocoderRequest request = new GeocoderRequest();
					for(LatLng l : markers) {
						Marker m = new Marker(map);
						m.setPosition(l);
					}
				}
				else {
					System.out.println(" PROBLEMAS "+status);
				}
			}

		});
	}
	
	
	
	
	public static void drawLines(LinkedList<LinkedList<LatLng>> lines) {
		MapViewOptions mvo = new MapViewOptions();
		mvo.importPlaces();
		// mvo.setApiKey("7a1y3jpxlg70k7covavndushu12jznd6puexhj0sc1q1truqbyyz5afwnqpkmtidk1kkspfoc9z0zt25");
		mvo.setApiKey("AIzaSyBj-RnKqf32h8bvdu15CWhO0K3zyt5eXw8");
		StreetViewMap mapView = new StreetViewMap();

		JFrame frame = new JFrame("Street View");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.add(mapView, BorderLayout.CENTER);
		frame.setSize(1600, 1000);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		mapView.setOnMapReadyHandler(new MapReadyHandler() {
			@Override
			public void onMapReady(MapStatus status) {
				System.out.println(" onMapReady "+status);

				if (status == MapStatus.MAP_STATUS_OK) {
					final Map map = mapView.getMap();
					map.setCenter(lines.get(0).get(0));

					map.setZoom(5.0);
					for(LinkedList<LatLng> line : lines) {
						Polyline p = new Polyline(map);
						LatLng path[] = new LatLng[line.size()];
						for (int  i = 0 ; i < line.size(); i++) {
							path[i] = line.get(i);
						}
						p.setPath(path);
					}
				}
				else {
					System.out.println(" PROBLEMAS "+status);
				}
			}

		});
	}
	
	public static void drawPointsAndLines(LinkedList<LatLng> markers,LinkedList<LinkedList<LatLng>> lines) {
		MapViewOptions mvo = new MapViewOptions();
		mvo.importPlaces();
		// mvo.setApiKey("7a1y3jpxlg70k7covavndushu12jznd6puexhj0sc1q1truqbyyz5afwnqpkmtidk1kkspfoc9z0zt25");
		mvo.setApiKey("AIzaSyBj-RnKqf32h8bvdu15CWhO0K3zyt5eXw8");
		StreetViewMap mapView = new StreetViewMap();

		JFrame frame = new JFrame("Street View");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.add(mapView, BorderLayout.CENTER);
		frame.setSize(1600, 1000);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		mapView.setOnMapReadyHandler(new MapReadyHandler() {
			@Override
			public void onMapReady(MapStatus status) {
				System.out.println(" onMapReady "+status);

				if (status == MapStatus.MAP_STATUS_OK) {
					final Map map = mapView.getMap();
					map.setCenter(lines.get(0).get(0));

					map.setZoom(5.0);
					for(LatLng l : markers) {
						Marker m = new Marker(map);
						m.setPosition(l);
					}
					for(LinkedList<LatLng> line : lines) {
						Polyline p = new Polyline(map);
						LatLng path[] = new LatLng[line.size()];
						for (int  i = 0 ; i < line.size(); i++) {
							path[i] = line.get(i);
						}
						p.setPath(path);
					}
				}
				else {
					System.out.println(" PROBLEMAS "+status);
				}
			}

		});
	}

	
	
}
