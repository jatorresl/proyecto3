package controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.Polyline;

import map.StreetViewMap;
import model.comparators.Comparator2A1;
import model.comparators.Comparator2A2;
import model.comparators.Comparator2A3;
import model.data_structures.BreadthFirstPaths;
import model.data_structures.CC;
import model.data_structures.Edge;
import model.data_structures.IQueue;
import model.data_structures.LinearProbingHashST;
import model.data_structures.MaxPQ;
import model.data_structures.Queue;

import model.data_structures.UndirectedGraph;
import model.data_structures.Vertice;
import model.util.Sort;
import model.vo.Haversine;
import model.vo.InfoEdge;
import model.vo.InfoVertice;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;
import view.StreetViewSingleton;

public class Controller {

	private LinearProbingHashST<String, VOMovingViolations> moviongViolations;

	private MovingViolationsManagerView view;

	private UndirectedGraph<InfoEdge, InfoVertice> map;

	double latitudMin, longitudMin, latitudMax, longitudMax;

	private Load load;

	/**
	 * Crea una instancia de la clase
	 */
	public Controller() {
		view = new MovingViolationsManagerView();
	}

	/**
	 * Metodo encargado de ejecutar los requerimientos segun la opcion indicada por
	 * el usuario
	 */
	public void run() {

		long startTime;
		long endTime;
		long duration;

		Scanner sc = new Scanner(System.in);
		boolean fin = false;

		while (!fin) {
			view.printMenu();

			int option = sc.nextInt();
			String idVertice1 = "";
			String idVertice2 = "";

			switch (option) {

			case 0:

				startTime = System.currentTimeMillis();
				load = new Load();
				map = load.getMap();
				latitudMin = load.getLatitudMin();
				latitudMax = load.getLatitudMax();
				longitudMin = load.getLongitudMin();
				longitudMax = load.getLongitudMax();
				moviongViolations = load.getMoviongViolations();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				// TODO Informar el total de vértices y el total de arcos que definen el grafo
				// cargado
				/*
				 * LinkedList<LatLng> markers = new LinkedList<>(); Iterator<String> keys =
				 * map.getVertices().iterator(); String clave; int count = 0; while
				 * (keys.hasNext() && count <10) { clave = keys.next(); Vertice<InfoVertice>
				 * vertice = map.getVertice(clave); double lat =
				 * vertice.getInfo().getLatitude(); double lon =
				 * vertice.getInfo().getLongitude(); LatLng marker = new LatLng(lat, lon);
				 * markers.add(marker); count++; }
				 * 
				 * StreetViewSingleton.drawPoints(markers);
				 **/
				break;

			case 1:

				view.printMessage("Ingrese El id del primer vertice (Ej. 901839): ");
				idVertice1 = sc.next();
				view.printMessage("Ingrese El id del segundo vertice (Ej. 901839): ");
				idVertice2 = sc.next();

				startTime = System.currentTimeMillis();
				caminoCostoMinimoA1(idVertice1, idVertice2);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				 * TODO Consola: Mostrar el camino a seguir con sus vértices (Id, Ubicación
				 * Geográfica), el costo mínimo (menor cantidad de infracciones), y la distancia
				 * estimada (en Km).
				 * 
				 * TODO Google Maps: Mostrar el camino resultante en Google Maps (incluyendo la
				 * ubicación de inicio y la ubicación de destino).
				 */
				break;

			case 2:
				view.printMessage(
						"2A. Consultar los N v�rtices con mayor n�mero de infracciones. Ingrese el valor de N: ");
				int n = sc.nextInt();

				startTime = System.currentTimeMillis();
				mayorNumeroVerticesA2(n);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				 * TODO Consola: Mostrar la informacion de los n vertices (su identificador, su
				 * ubicación (latitud, longitud), y el total de infracciones) Mostra el número
				 * de componentes conectadas (subgrafos) y los identificadores de sus vertices
				 * 
				 * TODO Google Maps: Marcar la localización de los vértices resultantes en un
				 * mapa en Google Maps usando un color 1. Destacar la componente conectada más
				 * grande (con más vértices) usando un color 2.
				 */
				break;

			case 3:

				view.printMessage("Ingrese El id del primer vertice (Ej. 901839): ");
				idVertice1 = sc.next();
				view.printMessage("Ingrese El id del segundo vertice (Ej. 901839): ");
				idVertice2 = sc.next();

				startTime = System.currentTimeMillis();
				caminoLongitudMinimoaB1(idVertice1, idVertice2);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");

				/*
				 * TODO Consola: Mostrar el camino a seguir, informando el total de vértices,
				 * sus vértices (Id, Ubicación Geográfica) y la distancia estimada (en Km).
				 * 
				 * TODO Google Maps: Mostre el camino resultante en Google Maps (incluyendo la
				 * ubicación de inicio y la ubicación de destino).
				 */
				break;

			case 4:
				double lonMin;
				double lonMax;
				view.printMessage("Ingrese la longitud minima (Ej. -87,806): ");
				lonMin = sc.nextDouble();
				view.printMessage("Ingrese la longitud m�xima (Ej. -87,806): ");
				lonMax = sc.nextDouble();

				view.printMessage("Ingrese la latitud minima (Ej. 44,806): ");
				double latMin = sc.nextDouble();
				view.printMessage("Ingrese la latitud m�xima (Ej. 44,806): ");
				double latMax = sc.nextDouble();

				view.printMessage("Ingrese el n�mero de columnas");
				int columnas = sc.nextInt();
				view.printMessage("Ingrese el n�mero de filas");
				int filas = sc.nextInt();

				startTime = System.currentTimeMillis();
				definirCuadriculaB2(lonMin, lonMax, latMin, latMax, columnas, filas);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");

				/*
				 * TODO Consola: Mostrar el número de vértices en el grafo resultado de la
				 * aproximación. Mostar el identificador y la ubicación geográfica de cada uno
				 * de estos vértices.
				 * 
				 * TODO Google Maps: Marcar las ubicaciones de los vértices resultantes de la
				 * aproximación de la cuadrícula en Google Maps.
				 */
				break;

			case 5:

				startTime = System.currentTimeMillis();
				arbolMSTKruskalC1();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				 * TODO Consola: Mostrar los vértices (identificadores), los arcos incluidos (Id
				 * vértice inicial e Id vértice final), y el costo total (distancia en Km) del
				 * árbol.
				 * 
				 * TODO Google Maps: Mostrar el árbol generado resultante en Google Maps: sus
				 * vértices y sus arcos.
				 */

				break;

			case 6:

				startTime = System.currentTimeMillis();
				arbolMSTPrimC2();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				 * TODO Consola: Mostrar los vértices (identificadores), los arcos incluidos (Id
				 * vértice inicial e Id vértice final), y el costo total (distancia en Km) del
				 * árbol.
				 * 
				 * TODO Google Maps: Mostrar el árbol generado resultante en Google Maps: sus
				 * vértices y sus arcos.
				 */
				break;

			case 7:

				startTime = System.currentTimeMillis();
				caminoCostoMinimoDijkstraC3();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				 * TODO Consola: Mostrar de cada camino resultante: su secuencia de vértices
				 * (identificadores) y su costo (distancia en Km).
				 * 
				 * TODO Google Maps: Mostrar los caminos de costo mínimo en Google Maps: sus
				 * vértices y sus arcos. Destaque el camino más largo (en distancia) usando un
				 * color diferente
				 */
				break;

			case 8:
				view.printMessage("Ingrese El id del primer vertice (Ej. 901839): ");
				idVertice1 = sc.next();
				view.printMessage("Ingrese El id del segundo vertice (Ej. 901839): ");
				idVertice2 = sc.next();

				startTime = System.currentTimeMillis();
				caminoMasCortoC4(idVertice1, idVertice2);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				 * TODO Consola: Mostrar del camino resultante: su secuencia de vértices
				 * (identificadores), el total de infracciones y la distancia calculada (en Km).
				 * 
				 * TODO Google Maps: Mostrar el camino resultante en Google Maps: sus vértices y
				 * sus arcos.
				 */
				break;

			case 9:
				fin = true;
				sc.close();
				break;
			}
		}
	}

	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia
	/**
	 * Requerimiento 1A: Encontrar el camino de costo m�nimo para un viaje entre dos
	 * ubicaciones geogr�ficas.
	 * 
	 * @param idVertice2
	 * @param idVertice1
	 */
	public void caminoCostoMinimoA1(String idVertice1, String idVertice2) {
		List<Vertice> list = map.getVerticesList();
		int rand1 = (int) Math.random() * list.size();
		int rand2 = (int) Math.random() * list.size();
		Vertice<InfoVertice> v1 = list.get(rand1);

		System.out.println("Punto 1");
		System.out.println("	Longitud: " + v1.getInfo().getLongitude());
		System.out.println("	Latitud: " + v1.getInfo().getLatitude());

		Vertice<InfoVertice> v2 = list.get(rand2);

		while (!v1.equals(v2)) {
			rand2 = (int) Math.random() * list.size();
			v2 = list.get(rand2);
		}

		System.out.println("Punto 2");
		System.out.println("	Longitud: " + v2.getInfo().getLongitude());
		System.out.println("	Latitud: " + v2.getInfo().getLatitude());
		/*
		 * DijkstraAlgorithm1A da1a = new DijkstraAlgorithm1A(map); da1a.execute(v1);
		 * LinkedList<Vertice> ll = da1a.getPath(v2); Iterator<Vertice> it =
		 * ll.iterator(); int infracciones = 0; double distancia = 0;
		 * Vertice<InfoVertice> previous = v1; int i = 1; while (it.hasNext()) {
		 * Vertice<InfoVertice> v = it.next();
		 * 
		 * System.out.println("Vertice " + i); System.out.println("	ID: " +
		 * v.getInfo().getId()); System.out.println("	Longitud: " +
		 * v.getInfo().getLongitude()); System.out.println("	Latitud: " +
		 * v.getInfo().getLatitude());
		 * 
		 * double long1 = v.getInfo().getLongitude(); double lat1 =
		 * v.getInfo().getLatitude(); double long2 = previous.getInfo().getLongitude();
		 * double lat2 = previous.getInfo().getLatitude(); double Ax = Math.abs(long1 -
		 * long2); double Ay = Math.abs(lat1 - lat2); double d = Math.sqrt(Math.pow(Ax,
		 * 2) + Math.pow(Ay, 2));
		 * 
		 * distancia += d; previous = v; infracciones += v.getInfo().getQueue().size();
		 * i++; }
		 * 
		 * System.out.println("N�mero de infracciones en el camino: " + infracciones);
		 * System.out.println("Distancia del camino: " + distancia);
		 */
		// TODO: Dibujar el camino
	}

	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia
	/**
	 * Requerimiento 2A: Determinar los n v�rtices con mayor n�mero de infracciones.
	 * Adicionalmente identificar las componentes conectadas (subgrafos) que se
	 * definan �nicamente entre estos n v�rtices
	 * 
	 * @param int n: numero de vertices con mayor numero de infracciones
	 */
	public Queue<InfoVertice> mayorNumeroVerticesA2(int n) {
		Comparator2A1 comparator = new Comparator2A1();
		MaxPQ<InfoVertice> pq = new MaxPQ<>(comparator);
		Iterator<String> it = map.getVertices().iterator();
		while (it.hasNext()) {
			String key = it.next();
			InfoVertice v = map.getInfoVertex(key);
			pq.insert(v);
		}

		Queue<InfoVertice> q = new Queue<>();
		int i = 0;
		try {
			while (i < n) {
				InfoVertice iv = pq.delMax();
				q.enqueue(iv);

				System.out.println("Vertice " + (i + 1));
				System.out.println("	ID: " + iv.getId());
				System.out.println("	Longitud: " + iv.getLongitude());
				System.out.println("	Latitud: " + iv.getLatitude());
				System.out.println("	Total de infracciones: " + iv.getQueue().size());

				i++;
			}
		} catch (NoSuchElementException e) {
			System.out.println("No hay m�s v�rtices (El top dado es mayor que el n�mero de v�rtices)");
		}

		CC cc = new CC(map);
		InfoVertice[] array = new InfoVertice[q.size()];
		i = 0;
		while (q.isEmpty()) {
			array[i] = q.dequeue();
			i++;
		}

		Comparator2A2 comp = new Comparator2A2(cc);
		Sort.quickSort3(array, 0, array.length - 1, comp);
		Queue<InfoVertice> tempQueue = new Queue<>();
		tempQueue.enqueue(array[0]);
		i = 1;
		int componentes = 1;
		Comparator2A3 compa = new Comparator2A3();
		MaxPQ<Queue<InfoVertice>> maxQueue = new MaxPQ<>(compa);
		while (i < array.length) {
			if (comp.compare(array[i], array[i - 1]) != 0) {
				maxQueue.insert(tempQueue);
				System.out.println("Componente Conectado" + componentes);
				System.out.println("	Vertices:");
				for (InfoVertice v : tempQueue)
					System.out.println("	- " + v.getId());
				componentes++;
				tempQueue = new Queue<>();
			}
			tempQueue.enqueue(array[i]);
			i++;
		}
		maxQueue.insert(tempQueue);
		System.out.println("Componente Conectado" + componentes);
		System.out.println("	Vertices:");
		for (InfoVertice v : tempQueue)
			System.out.println("	- " + v.getId());
		tempQueue = new Queue<>();

		return maxQueue.delMax();
		// TODO: Informar componentes conectadas y dibujarlas en el mapa
	}

	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia
	/**
	 * Requerimiento 1B: Encontrar el camino m�s corto para un viaje entre dos
	 * ubicaciones geogr�ficas
	 * 
	 * @param idVertice2
	 * @param idVertice1
	 */
	public void caminoLongitudMinimoaB1(String idVertice1, String idVertice2) {
		BreadthFirstPaths<InfoEdge, InfoVertice> breadthFirst = new BreadthFirstPaths<>(map, idVertice1, idVertice2);
		ArrayList<String> path = breadthFirst.getPath();
		LinkedList<LatLng> markers = new LinkedList<>();
		int tam = path.size();
		double distance = 0;
		LinkedList<LinkedList<LatLng>> lineas = new LinkedList<>();
		for (int i = 0; i < tam - 1; i++) {
			Vertice<InfoVertice> vertex1 = map.getVertice(path.get(i));
			System.out.println(vertex1.getInfo().toString());
			Vertice<InfoVertice> vertex2 = map.getVertice(path.get(i + 1));
			
			double lat = vertex1.getInfo().getLatitude();
			double lon = vertex1.getInfo().getLongitude();
			double lat2 = vertex2.getInfo().getLatitude();
			double lon2 = vertex2.getInfo().getLongitude();
			LatLng ll1 = new LatLng(lat, lon);
			LatLng ll2 = new LatLng(lat2, lon2);
			markers.add(ll1);
			LinkedList<LatLng> linea = new LinkedList<>();
			Edge<InfoEdge, InfoVertice> edge = map.getEdge(vertex1.getKey(), vertex2.getKey());
			distance += edge.getCost();
			linea.add(ll1);
			linea.add(ll2);
			
			
			lineas.add(linea);
		}
		Vertice<InfoVertice> vertex1 = map.getVertice(path.get(tam - 1));
		double lat = vertex1.getInfo().getLatitude();
		double lon = vertex1.getInfo().getLongitude();
		markers.add(new LatLng(lat, lon));

		StreetViewSingleton.drawPointsAndLines(markers, lineas);

		System.out.println("El numero de vertices es " + path.size());
		System.out.println("la distancia en es de "+ distance);
	}

	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia
	/**
	 * Requerimiento 2B: Definir una cuadricula regular de N columnas por M filas.
	 * que incluya las longitudes y latitudes dadas
	 * 
	 * @param lonMin: Longitud minima presente dentro de la cuadricula
	 * @param lonMax: Longitud maxima presente dentro de la cuadricula
	 * @param latMin: Latitud minima presente dentro de la cuadricula
	 * @param latMax: Latitud maxima presente dentro de la cuadricula
	 * @param columnas: Numero de columnas de la cuadricula
	 * @param filas: Numero de filas de la cuadricula
	 */
	public void definirCuadriculaB2(double lonMin, double lonMax, double latMin, double latMax, int n, int m) {
		// TODO Auto-generated
		double difLon = lonMax - lonMin;
		double difLat = latMax - latMin;
		double x = difLon / n;
		double y = difLat / m;

		IQueue<String> vertices = new Queue<>();
		for (double i = lonMin; i <= lonMax; i += x) {
			for (double j = latMin; j <= latMax; j += y) {
				double nearest = Double.MAX_VALUE;
				String nearestKey = "";
				Iterator<String> keys = map.getVertices().iterator();
				String clave;
				while (keys.hasNext()) {
					clave = keys.next();
					Vertice<InfoVertice> vertice = map.getVertice(clave);
					double lat = vertice.getInfo().getLatitude();
					double lon = vertice.getInfo().getLongitude();
					double distance = Haversine.distance(j, i, lat, lon);
					if (distance < nearest) {
						nearest = distance;
						nearestKey = clave;
					}
				}
				vertices.enqueue(nearestKey);
			}
		}

		System.out.println("numero de vertices "+ vertices.size());
		LinkedList<LatLng> markers = new LinkedList<>();
		while(!vertices.isEmpty()) {
			String llave = vertices.dequeue();
			Vertice<InfoVertice> vertex =map.getVertice(llave);
			System.out.println(vertex.getInfo().toString());
			double lat = vertex.getInfo().getLatitude();
			double lon = vertex.getInfo().getLongitude();
			markers.add(new LatLng(lat, lon));
		}
		StreetViewSingleton.drawPoints(markers);
	}

	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia
	/**
	 * Requerimiento 1C: Calcular un �rbol de expansi�n m�nima (MST) con criterio
	 * distancia, utilizando el algoritmo de Kruskal.
	 */
	public void arbolMSTKruskalC1() {
		// TODO Auto-generated method stub

	}

	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia
	/**
	 * Requerimiento 2C: Calcular un �rbol de expansi�n m�nima (MST) con criterio
	 * distancia, utilizando el algoritmo de Prim. (REQ 2C)
	 */
	public void arbolMSTPrimC2() {
		// TODO Auto-generated method stub

	}

	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia
	/**
	 * Requerimiento 3C: Calcular los caminos de costo m�nimo con criterio distancia
	 * que conecten los v�rtices resultado de la aproximaci�n de las ubicaciones de
	 * la cuadricula N x M encontrados en el punto 5.
	 */
	public void caminoCostoMinimoDijkstraC3() {
		// TODO Auto-generated method stub

	}

	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la conveniencia
	/**
	 * Requerimiento 4C:Encontrar el camino m�s corto para un viaje entre dos
	 * ubicaciones geogr�ficas escogidas aleatoriamente al interior del grafo.
	 * 
	 * @param idVertice2
	 * @param idVertice1
	 */
	public void caminoMasCortoC4(String idVertice1, String idVertice2) {
		// TODO Auto-generated method stub

	}
}
