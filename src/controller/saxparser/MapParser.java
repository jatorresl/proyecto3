package controller.saxparser;

import java.util.LinkedList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import model.data_structures.UndirectedGraph;
import model.data_structures.Vertice;
import model.vo.Haversine;
import model.vo.InfoEdge;
import model.vo.InfoVertice;

public class MapParser extends DefaultHandler {
	UndirectedGraph<InfoEdge, InfoVertice> map;
	Way currentWay = null;

	int count = 0;
	public MapParser(UndirectedGraph<InfoEdge, InfoVertice> map) {
		this.map = map;
		currentWay = null;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (qName.equals("way")) {
			currentWay = new Way(Long.parseLong(attributes.getValue("id")));
		}
		else if (qName.equals("nd") && currentWay != null) {
			currentWay.addVertice(attributes.getValue("ref"));
		}
		else if (qName.equals("tag") && attributes.getValue("k").equals("highway") && currentWay != null) {
			currentWay.highway = true;
		}
	}
	
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (qName.equals("way") && currentWay.highway) {
			int tam = currentWay.vertices.size();
			for (int i = 0; i < tam-1; i++) {
				
				Vertice<InfoVertice> vertex1 = map.getVertice(currentWay.vertices.get(i));
				Vertice<InfoVertice> vertex2 = map.getVertice(currentWay.vertices.get(i+1));
				InfoEdge info = new InfoEdge();
				double cost =  vertex1.getInfo().haversineDist(vertex2.getInfo());
				map.addEdge(vertex1, vertex2, info, cost);
				count++;
			}
		}
		if (qName.equals("way")) {
			currentWay = null;
		}
		
	}

	class Way {
		long id;
		LinkedList<String> vertices;
		boolean highway;

		public Way(long id) {
			this.id = id;
			vertices = new LinkedList<>();
		}
		public void addVertice(String v) {
			vertices.add(v);
		}
	}
}
