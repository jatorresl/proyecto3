package model.vo;


/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations>
{
	private int objectID;

	private String location;

	private int addressID;

	private double streetSegID;

	private String ticketType;

	private int fineamt;

	private double totalPaid;

	private int penalty1;

	private String accidentIndicator;

	private String ticketIssueDate;

	private String violationCode;

	private String violationDesc;
	
	private double latitude;
	
	private double longitude;

	public VOMovingViolations(int objectID, String location, int addressID, double streetSegID, String ticketType, int fineamt, double totalPaid, int penalty1, String accidentIndicator,
			String ticketIssueDate, String violationCode, String violationDesc, double latitude, double longitude) {
		super();
		this.objectID = objectID;
		this.location = location;
		this.addressID = addressID;
		this.streetSegID = streetSegID;
		this.ticketType = ticketType;
		this.fineamt = fineamt;
		this.totalPaid = totalPaid;
		this.penalty1 = penalty1;
		this.accidentIndicator = accidentIndicator;
		this.ticketIssueDate = ticketIssueDate;
		this.violationCode = violationCode;
		this.violationDesc = violationDesc;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public int getObjectID() {
		return objectID;
	}

	public void setObjectID(int objectID) {
		this.objectID = objectID;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getAddressID() {
		return addressID;
	}

	public void setAddressID(int addressID) {
		this.addressID = addressID;
	}

	public double getStreetSegID() {
		return streetSegID;
	}

	public void setStreetSegID(double streetSegID) {
		this.streetSegID = streetSegID;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public int getFineamt() {
		return fineamt;
	}

	public void setFineamt(int fineamt) {
		this.fineamt = fineamt;
	}

	public double getTotalPaid() {
		return totalPaid;
	}

	public void setTotalPaid(double totalPaid) {
		this.totalPaid = totalPaid;
	}

	public int getPenalty1() {
		return penalty1;
	}

	public void setPenalty1(int penalty1) {
		this.penalty1 = penalty1;
	}

	public String getAccidentIndicator() {
		return accidentIndicator;
	}

	public void setAccidentIndicator(String accidentIndicator) {
		this.accidentIndicator = accidentIndicator;
	}

	public String getTicketIssueDate() {
		return ticketIssueDate;
	}

	public void setTicketIssueDate(String ticketIssueDate) {
		this.ticketIssueDate = ticketIssueDate;
	}

	public String getViolationCode() {
		return violationCode;
	}

	public void setViolationCode(String violationCode) {
		this.violationCode = violationCode;
	}

	public String getViolationDesc() {
		return violationDesc;
	}

	public void setViolationDesc(String violationDesc) {
		this.violationDesc = violationDesc;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public int compareTo(VOMovingViolations o) {

		return objectID- o.getAddressID();
	}
}
