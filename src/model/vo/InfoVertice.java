package model.vo;

import model.data_structures.IQueue;
import model.data_structures.Queue;

public class InfoVertice implements Comparable<InfoVertice> {
	
	private String id;
	
	private double  latitude;
	
    private double longitude;
    
    private Queue<VOMovingViolations> queue;

    public InfoVertice() {
    	queue = new Queue<>();
    }
    
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public void addMoviongViolation(VOMovingViolations p) {
		queue.enqueue(p);
	}

	public Queue<VOMovingViolations> getQueue() {
		return queue;
	}
	
	public void setQueue(Queue<VOMovingViolations> queue) {
		this.queue = queue;
	}

	public String toString() {
		return "id="+id+" lat="+latitude+" lon="+longitude;		
	}
	
	public double haversineDist(InfoVertice vertice2) {
		double long1 = getLongitude();
		double lat1 = getLatitude();
		double long2 = vertice2.getLongitude();
		double lat2 = vertice2.getLatitude();
		return Haversine.distance(lat1, long1, lat2, long2);
	}

	@Override
	public int compareTo(InfoVertice arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
}
