package model.comparators;

import java.util.Comparator;

import model.data_structures.CC;
import model.vo.InfoVertice;

public class Comparator2A2 implements Comparator<InfoVertice> {

	private CC cc;
	
	public Comparator2A2(CC c) {
		cc = c;
	}
	
	public int compare(InfoVertice i1, InfoVertice i2) {
		if (cc.id(i1.getId()) > cc.id(i2.getId()))
			return 1;
		else if (cc.id(i1.getId()) < cc.id(i2.getId()))
			return -1;
		else
			return 0;
	}

}
