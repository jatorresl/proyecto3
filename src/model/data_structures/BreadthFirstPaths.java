




























































































































package model.data_structures;

import java.util.ArrayList;


public class BreadthFirstPaths<A, V> {
    
	private UndirectedGraph<A, V> graph;
	private boolean havePath;
    private String[] explored;  // marked[v] = is there an s-v path
    private int numExplored;
    private IQueue<ArrayList<String>> queue;

    private String vertice2;
    private ArrayList<String> path;

    /**
     * Computes the shortest path be
     * tween the source vertex {@code s}
     * and every other vertex in the graph {@code G}.
     * @param G the graph
     * @param s the source vertex
     * @throws IllegalArgumentException unless {@code 0 <= s < V}
     */
    public BreadthFirstPaths(UndirectedGraph<A, V> G, String vertice1 , String vertice2 ) {
        explored = new String[G.V()];
        this.path = new ArrayList<>();
        this.havePath = false;
        this.graph = G;
        this.vertice2 = vertice2;
        ArrayList<String> x = new ArrayList<>();
        x.add(vertice1);
        queue = new Queue<>();
        queue.enqueue(x);
        numExplored = 0;       
    }

    public ArrayList<String> getPath(){
    	findpath();
    	if(!havePath) {
    		System.out.println("No se encontro un camino");
    	}
    	return path;
    
    }
    private void findpath(){
    	
    	while( (!havePath) && (!queue.isEmpty()) ) {		
    		ArrayList<String> tempPath = queue.dequeue();
        	int last =  tempPath.size()-1;
        	String key = tempPath.get(last);
        	Vertice<V> node = graph.getVertice(key);
        	ArrayList<String> adj = node.getAdj();
        	System.out.println(" --> " +key);
        	
        	for (String string : adj) {
        		
        		if(!isExplored(string)) {
        			ArrayList<String > newPath = generatePath(tempPath, string);
        			if(string.equals(vertice2)) {
        				path = newPath;
        				havePath = true;
        			}
        			else {
        				queue.enqueue(newPath);
        			}
        		}
    		}
        	explored[numExplored] = node.getKey(); 
        	numExplored++;
    	}
    	
    }
    
    public boolean isExplored(String vertex) {
    	boolean isExplored = false;
    	for (int i = 0; i < numExplored; i++) {
			if(vertex.equals(explored[i])) {
				isExplored = true;
				return isExplored;
			}
		}
    	return isExplored;
    }

    public ArrayList<String> generatePath(ArrayList<String> arr, String vertex){
    	ArrayList<String> newPath = new ArrayList<>();
    	int tam = arr.size();
    	for (int i = 0; i < tam; i++) {
			String value = arr.get(i);
			newPath.add(value);
		}
    	newPath.add(vertex);
    	return newPath;
    }

}
