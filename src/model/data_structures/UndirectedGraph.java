package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import model.vo.Haversine;
import model.vo.InfoVertice;

public class UndirectedGraph<A, V> {

	private int numEdges;

	private int numVertices;

	private LinkedList<Edge> edgeList;

	private SeparateChainingHashST<String , Vertice> hash;

	public UndirectedGraph() {

		numEdges = 0;
		numVertices = 0;
		hash = new SeparateChainingHashST<>();
		edgeList = new LinkedList<>();
	}

	
	public Vertice getVertice(String key) {
		return hash.get(key);
	}
	
	public Iterable<String> getVertices() {
		return hash.keys();
	}
		
	public List<Vertice> getVerticesList() {
		Iterator<String> it = hash.keys().iterator();
		ArrayList<Vertice> array = new ArrayList<>();
		
		while (it.hasNext())
		{
			String key = it.next();
			array.add(hash.get(key));
		}
		
		return array;
	}
	
	/**
	 * Da el numero de vertices que tiene el grafo
	 * @return numero de vertices
	 */
	public int V() {
		return numVertices;
	}
	
	/**
	 * Da el numero de arcos que tiene el grafo
	 * @return numero de arcos 
	 */
	public int E() {
		return numEdges;
	}
	
	public List<Edge> getEdgesList()
	{
		ArrayList<Edge> al = new ArrayList<>();
		
		int i = 0;
		while (i < edgeList.size())
		{
			al.add(edgeList.get(i));
			i++;
		}
		
		return al;
	}
	
	/**
	 * Agrega un vertice nuevo el arco
	 * @param key
	 * @param vertice
	 */
	public void addVertice(String key, Vertice vertice ) {
		hash.put(key, vertice);
		numVertices++;
	}

	/**
	 * Agrega un arco nuevo al grafo
	 * @param id1
	 * @param id2
	 * @param info
	 * @param cost
	 */
	public void addEdge(Vertice<V> id1, Vertice<V> id2, A info, double cost ) {
		Vertice<V> vertice1 = id1;
		Vertice<V> vertice2 = id2;

		Edge<A,V> edge = new Edge<A,V>(cost, info, vertice1, vertice2);
		vertice1.addAdj(vertice2.getKey());
		vertice2.addAdj(vertice1.getKey());
		edgeList.add(edge);
		numEdges++;
	}
	
	/**
	 * Da la informacion del vertice dado
	 * @param id
	 * @return V
	 */
	public V getInfoVertex(String id ) {
		Vertice<V> vertex = hash.get(id);
		return vertex.getInfo();
	}
	
	/**
	 * Se cambia l ainformcion del vertice dado por la dada por parametro
	 * @param idVertex
	 * @param infoVertex
	 */
	public void setInfoVertex(String idVertex, V infoVertex){
		Vertice<V> vertex = hash.get(idVertex);
		vertex.setInfo(infoVertex);
	}
	
	/**
	 * Da la informacion del arco que se encuentra entre los dos arcos dados
	 * @param id1
	 * @param id2
	 * @return Informacion del arco
	 */
	public A getInfoEdge(String id1, String id2 ) {
		A info = null;
		for (Edge edge : edgeList) {
			String idVertex1 = edge.getVertice1().getKey();
			String idVertex2 = edge.getVertice2().getKey();
			if( (idVertex1.equals(id1) && idVertex2.equals(id2)) || (idVertex1.equals(id2) && idVertex2.equals(id1)) ) {
				info =  (A) edge.getInfo();
				return info;
		
			}
		}
		return info;
	}
	
	public Edge<A, V> getEdge(String id1, String id2 ) {
		A info = null;
		for (Edge edge : edgeList) {
			String idVertex1 = edge.getVertice1().getKey();
			String idVertex2 = edge.getVertice2().getKey();
			if( (idVertex1.equals(id1) && idVertex2.equals(id2)) || (idVertex1.equals(id2) && idVertex2.equals(id1)) ) {
				info =  (A) edge.getInfo();
				return edge;
		
			}
		}
		return null;
	}
	
	
	
	/**
	 * Cambia la informacion del arco que se encuentra entre los dos vertices dados 
	 * @param id1
	 * @param id2
	 * @param info
	 */
	public void setInfoEdge(String id1, String id2, A info ) {
		for (Edge edge : edgeList) {
			String idVertex1 = edge.getVertice1().getKey();
			String idVertex2 = edge.getVertice2().getKey();
			if( (idVertex1.equals(id1) && idVertex2.equals(id2)) || (idVertex1.equals(id2) && idVertex2.equals(id1)) ) {
				edge.setInfo(info);
				return;
			}
		}
	}
	
	/**
	 * Retorna los identificadores de los vertices adyacentes al vertice dado por parametro
	 * @param idVertex
	 * @return Identificadores de los vertices adyacentes
	 */
	public Iterable<String> adj(String idVertex){
		IQueue<String> queue = new Queue<>();
		ArrayList<Vertice<V>> vertex =  hash.get(idVertex).getAdj();
		for (Vertice<V> vertice : vertex) {
			queue.enqueue(vertice.getKey());
		}
		return queue;
	}
}
