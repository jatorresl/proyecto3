package model.data_structures;

import javax.xml.ws.handler.HandlerResolver;

import model.vo.Haversine;
import model.vo.InfoVertice;
import model.vo.VOMovingViolations;

public class Edge<A,V> {

	private double cost;
	
	private A info;
	
	private Vertice<V> vertice1;
	
	private Vertice<V> vertice2;

	public double getCost() {
		return cost;
	}

	public Edge(double cost, A info, Vertice<V> vertice1, Vertice<V> vertice2) {
		super();
		this.cost = cost;
		this.info = info;
		this.vertice1 = vertice1;
		this.vertice2 = vertice2;
	}

	public A getInfo() {
		return info;
	}

	public void setInfo(A info) {
		this.info = info;
	}

	public Vertice<V> getVertice1() {
		return vertice1;
	}

	public Vertice<V> getVertice2() {
		return vertice2;
	}
}